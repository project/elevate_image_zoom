Elevate Image Zoom
------------------

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Support
 * Maintainers

INTRODUCTION
------------

The Elevate image zoom provides a field formatter that allows you to apply it to
your image field. This will provide different types of image zoom features.
Zoom types are as follow
 * Basic zoom
 * Tint zoom
 * Inner zoom
 * Lens zoom
 * Mouse wheel zoom

REQUIREMENTS
------------
Elevate image zoom Depends upon elevate zoom library so you have to Download
the elevate zoom library and save it to /libraries/elevatezoom
link :- http://www.elevateweb.co.uk/download-file

INSTALLATION
------------

 * Extract module at drupal/modules/contrib directory.
 * This module depends upon elevate zoom library so you have to Download the
   elevate zoom library and save it to /libraries/elevatezoomand enable it from
   browser
 * After saving it; you can enable the Elevate image zoom module by going in
   this path /admin/modules.

SUPPORT
-------

This open source project is supported by the Drupal.org community. To report a
bug, request a feature, or upgrade to the latest version, please visit the
project page: http://drupal.org/project/elevate_image_zoom

MAINTAINERS
-----------

Keshav K (keshavk)
https://www.drupal.org/u/keshavk
